﻿Feature: Withdraw Amount
	In order to Get My Money Back From My Wallet
    As a trader,
    I want to be able to withdraw amount 


Background:
	Given A verified user is logged in
	And Navigated to withdraw screen


Scenario: Withdraw Should Failed When There isn't Sufficient Amount
    Given meisam has '100' unit available in his 'IRR' account balance
    When he tries to Withdraw '1000' unit from his wallet
    Then he should see the withdraw InSufficient Balance Exception


Scenario: Withdraw Should Passed When There is Sufficient Amount
    Given salman has 100100 unit available in his 'IRR' account balance
    When he tries to Withdraw 1000 unit from his wallet
    Then he should see the withdraw Success Message
    And he should see his 'IRR' account with '99100' available
