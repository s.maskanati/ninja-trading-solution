﻿Feature: DepositFundsToWallet
	In order to deposit funds to the wallet
	As a trader
	I want to be able to deposit amount to my wallet

Background:
	Given A verified user is logged in
	And Navigated to Deposit screen
	And The default currency is IRR

@happy
Scenario: Deposit money
	Given My bank account is empty
	When I deposit 100 units
	Then The account balance should be 100 unit

@happy
Scenario: Negative amount deposit
	Given My bank account is empty
	When  I try to deposit a negative amount
	Then I get an exception - FAILED

@happy
Scenario Outline: User wants to deposit money with a significant currency
	Given My bank account is empty
	When I chooses the <currency>
	And I enter 50 unit as deposit amount
	Then The account balance should be 50 unit as <currency>
	And The account balance should be 50 multiplied by <exchange_rate>

	Examples:
		| currency	| exchange_rate		|
		| USD		| 200000			|
		| IRR		| 1					|
		| ‏EUR		| 250000			|		
